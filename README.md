# Tk

Tk is a cross-platform GUI toolkit implemented with the Tcl scripting language.


## Installation

    $ go get modernc.org/tk

## Linking using ccgo

    $ ccgo foo.c -lmodernc.org/tk/lib

## Documentation

[godoc.org/modernc.org/tk](http://godoc.org/modernc.org/tk)

## Builders

[modern-c.appspot.com/-/builder/?importpath=modernc.org%2ftk](https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2ftk)

## Demo

![Showcase](https://drive.google.com/uc?id=1hFolhGST6oQdqNzeq6K1yOsL00X-2c7D)

// Copyright 2021 The Tk-Go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import "modernc.org/tk"

func main() {
	app := tk.AppM()
	label := app.NewTLabelM().SetTextM("Hello, World!")
	app.PackM(label).MainM()
}

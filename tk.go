// Copyright 2021 The Tk-Go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Many parts of the Go API documentation are copied and/or modified from the
// online Tcl/Tk documentation site at
//
// 	https://www.tcl.tk/man/tcl8.6/contents.html
//
// Please see the LICENSE-TK-DOCS file for details.

//go:generate go run generator.go
//go:generate assets -o internal/lib/assets.go -package lib -dir

package tk // import "modernc.org/tk"

//TODO Some standard options still have no docs.
//TODO Generate also a mechanical set/get roundtrip test where applicable.
//TODO Last option done: Canvas.YScrollCommand

import (
	"fmt"
	"os"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"

	"github.com/cosmos72/gls"
	"modernc.org/libc"
	"modernc.org/tcl"
	tcllib "modernc.org/tcl/lib"
	"modernc.org/tk/internal/lib"
	"modernc.org/tk/lib"
)

const (
	queueSize = 10
	_         = 1 / queueSize // Check queueSize != 0
)

var (
	_ Widget = (*Application)(nil)

	app Application

	_ = &app.noCopy
	_ = todo
	_ = trc
)

func origin(skip int) string {
	pc, fn, fl, _ := runtime.Caller(skip)
	f := runtime.FuncForPC(pc)
	var fns string
	if f != nil {
		fns = f.Name()
		if x := strings.LastIndex(fns, "."); x > 0 {
			fns = fns[x+1:]
		}
	}
	return fmt.Sprintf("%s:%d:%s", fn, fl, fns)
}

func todo(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s: TODO %s", origin(2), s) //TODOOK
	fmt.Fprintf(os.Stdout, "%s\n", r)
	os.Stdout.Sync()
	return r
}

func trc(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s: TRC %s", origin(2), s)
	fmt.Fprintf(os.Stdout, "%s\n", r)
	os.Stdout.Sync()
	return r
}

type interp struct {
	*tcl.Interp
}

func newInterp(tclLibPath, tkLibPath string) (r *interp, err error) {
	r = &interp{}
	if r.Interp, err = tcl.NewInterp(); err != nil {
		return nil, err
	}

	if tclLibPath != "" {
		if _, err := r.eval(fmt.Sprintf("set tcl_library {%s}", tclLibPath)); err != nil {
			return nil, err
		}
	}

	if tkLibPath != "" {
		if _, err := r.eval(fmt.Sprintf("set tk_library {%s}", tkLibPath)); err != nil {
			return nil, err
		}
	}

	if rc := tcllib.XTcl_Init(r.TLS(), r.Handle()); rc != tcllib.TCL_OK {
		return nil, fmt.Errorf("%s (rc %d)", r.stringResult(), rc)
	}

	return r, nil
}

func (in *interp) stringResult() string {
	return libc.GoString(tcllib.XTcl_GetStringResult(in.TLS(), in.Handle()))
}

func (in *interp) eval(s string) (r string, err error) {
	p, err := libc.CString(s)
	if err != nil {
		return "", err
	}

	defer libc.Xfree(in.TLS(), p)

	if rc := tcllib.XTcl_Eval(in.TLS(), in.Handle(), p); rc != tcllib.TCL_OK {
		return "", fmt.Errorf("%s (rc %d)", in.stringResult(), rc)
	}

	return in.stringResult(), nil
}

func (in *interp) getChan(name string) (uintptr, error) {
	cs, err := libc.CString(name)
	if err != nil {
		return 0, err
	}

	defer libc.Xfree(in.TLS(), cs)

	r := tcllib.XTcl_GetChannel(in.TLS(), in.Handle(), cs, 0)
	if r == 0 {
		return 0, fmt.Errorf("getChan(%q): not found", name)
	}

	return r, nil
}

func bool2int(b bool) int {
	if b {
		return 1
	}

	return 0
}

type msg struct {
	err      error
	request  string
	response chan *msg
	result   string
}

// MountLibraryVFS mounts the tk library virtual file system and returns the
// mount point.
func MountLibraryVFS() (string, error) {
	point := "usr/lib/tk8.6"
	if err := tcl.MountFileSystem(point, lib.Assets()); err != nil {
		return "", err
	}

	return point, nil
}

// Application represents the Tk application. It also implements Widget and the
// Window() method returns the root window.
type Application struct {
	// https://github.com/golang/go/issues/8005#issuecomment-190753527
	noCopy sync.Mutex

	*Window
	appMessages   chan *msg
	gid           uintptr
	initErr       error
	initOnce      sync.Once
	interp        *interp
	mainExited    chan struct{}
	startMain     chan struct{}
	startNotifier chan struct{}
	tkMessages    chan *msg

	serial uint32
}

// App returns the Application singleton or an error, if any.
func App() (r *Application, err error) {
	app.initOnce.Do(func() {
		app.init()
	})
	if app.initErr != nil {
		return nil, err
	}

	return &app, nil
}

// AppM is like App but panics on errors.
func AppM() *Application {
	r, err := App()
	if err != nil {
		panic(err)
	}

	return r
}

func (a *Application) newID() string {
	return fmt.Sprint(atomic.AddUint32(&a.serial, 1))
}

// Main repeatedly handles events. It returns only when there are no main
// windows left. Most windowing applications will call Main after
// initialization; the main execution of the application will consist entirely
// of callbacks.
//
// Calling Main more than once panics.
func (a *Application) Main() error {
	close(a.startMain)
	<-a.mainExited
	return nil
}

// MainM is like Main but panics on error.
func (a *Application) MainM() {
	if err := a.Main(); err != nil {
		panic(err)
	}
}

func (a *Application) eval(s string) (string, error) {
	if gls.GoID() == a.gid {
		return a.interp.eval(s)
	}

	ch := make(chan *msg, 1)
	appMessage := &msg{request: s, response: ch}
	a.appMessages <- appMessage
	appMessage = <-appMessage.response
	return appMessage.result, appMessage.err
}

func (a *Application) evalBool(s string) (bool, error) {
	r, err := a.eval(s)
	if err != nil {
		return false, err
	}

	return tclBool2bool(r)
}

func (a *Application) evalFloat(s string) (float64, error) {
	r, err := a.eval(s)
	if err != nil {
		return 0, err
	}

	return strconv.ParseFloat(r, 64)
}

func (a *Application) evalInt(s string) (int, error) {
	r, err := a.eval(s)
	if err != nil {
		return 0, err
	}

	return strconv.Atoi(r)
}

func (a *Application) init() {
	a.mainExited = make(chan struct{})
	a.startMain = make(chan struct{})
	a.startNotifier = make(chan struct{})
	initialized := make(chan struct{})

	go func() {
		runtime.LockOSThread()
		a.gid = gls.GoID()
		a.initErr = a.initTk()
		a.Window = &Window{app: a, path: "."}
		initialized <- struct{}{}
		if a.initErr != nil {
			return
		}

		for {
			select {
			case <-a.startMain:
				close(a.startNotifier)
				tk.XTk_MainLoop(a.interp.TLS())
				close(a.mainExited)
				return
			case appMessage := <-a.appMessages:
				appMessage.result, appMessage.err = a.interp.eval(appMessage.request)
				appMessage.response <- appMessage
			}
		}
	}()

	<-initialized
}

func (a *Application) initTk() (err error) {
	tclLibPath, err := tcl.MountLibraryVFS()
	if err != nil {
		return err
	}

	if err := os.Setenv("TCL_LIBRARY", tclLibPath); err != nil {
		return err
	}

	tkLibPath, err := MountLibraryVFS()
	if err != nil {
		return err
	}

	if err := os.Setenv("TK_LIBRARY", tkLibPath); err != nil {
		return err
	}

	if a.interp, err = newInterp(tclLibPath, tkLibPath); err != nil {
		return err
	}

	if rc := tk.XTk_Init(a.interp.TLS(), a.interp.Handle()); rc != tk.TCL_OK {
		return fmt.Errorf("%s (rc %d)", a.interp.stringResult(), rc)
	}

	if _, err = a.interp.NewCommand("messageHandler", a.messageHandler, a.interp, nil); err != nil {
		return err
	}

	notify, err := a.interp.eval(`
set pipe [chan pipe]
set notified [lindex $pipe 0]
set notify [lindex $pipe 1]
chan configure $notify -buffering line

chan event $notified readable {
	chan gets $notified
	messageHandler
}

set notify
`)

	if err != nil {
		return err
	}

	tclChan, err := a.interp.getChan(notify)
	if err != nil {
		return err
	}

	in, err := newInterp(tclLibPath, tkLibPath)
	if err != nil {
		return err
	}

	a.appMessages = make(chan *msg, queueSize)
	a.tkMessages = make(chan *msg, queueSize)
	tcllib.XTcl_RegisterChannel(in.TLS(), in.Handle(), tclChan)

	go a.notifier(in, notify)

	return nil
}

func (a *Application) messageHandler(clientData interface{}, _ *tcl.Interp, args []string) int {
	in := clientData.(*interp)
	tkMessage := <-a.tkMessages
	tkMessage.result, tkMessage.err = in.eval(tkMessage.request)
	tkMessage.response <- tkMessage
	return tcllib.TCL_OK
}

func (a *Application) notifier(in *interp, notify string) {
	notification := fmt.Sprintf("chan puts {%s} {}", notify)
	<-a.startNotifier
	for {
		select {
		case <-a.mainExited:
			return
		case appMessage := <-a.appMessages:
			a.tkMessages <- appMessage
			in.eval(notification)
		}
	}
}

// SetStrictMotif sets the tk_strictMotif variable. It is set to false by
// default. If an application sets it to true, then Tk attempts to adhere as
// closely as possible to Motif look-and-feel standards. For example, active
// elements such as buttons and scrollbar sliders will not change color when
// the pointer passes over them. Modern applications should not normally set
// this variable.
func (a *Application) SetStrictMotif(value bool) error {
	_, err := a.eval(fmt.Sprintf("set tk_strictMotif %d", bool2int(value)))
	return err
}

// SetStrictMotifM is like SetStrictMotif but panics on error.
func (a *Application) SetStrictMotifM(value bool) *Application {
	if err := a.SetStrictMotif(value); err != nil {
		panic(err)
	}

	return a
}

// StrictMotif returns the current value of the tk_strictMotif variable.
func (a *Application) StrictMotif() (bool, error) {
	return a.evalBool("set tk_strictMotif")
}

// StrictMotifM is like StrictMotif but panics on error.
func (a *Application) StrictMotifM() bool {
	r, err := a.StrictMotif()
	if err != nil {
		panic(err)
	}

	return r
}

// Font wraps the Tk font command, providing several facilities for dealing
// with fonts, such as defining named fonts and inspecting the actual
// attributes of a font. The command arguments have several different forms,
// determined by the first argument. The following forms are currently
// supported:
//
// 	actual font ?-displayof window? ?option? ?--? ?char?
//
// Returns information about the actual attributes that are obtained when font
// is used on window's display; the actual attributes obtained may differ from
// the attributes requested due to platform-dependent limitations, such as the
// availability of font families and point sizes. font is a font description;
// see "Font Descriptions" below. If the window argument is omitted, it defaults
// to the main window. If option is specified, returns the value of that
// attribute; if it is omitted, the return value is a list of all the
// attributes and their values. See "Font Options" below for a list of the
// possible attributes. If the char argument is supplied, it must be a single
// character. The font attributes returned will be those of the specific font
// used to render that character, which will be different from the base font if
// the base font does not contain the given character. If char may be a hyphen,
// it should be preceded by -- to distinguish it from a misspelled option.
//
// 	configure fontname ?option? ?value option value ...?
//
// Query or modify the desired attributes for the named font called fontname.
// If no option is specified, returns a list describing all the options and
// their values for fontname. If a single option is specified with no value,
// then returns the current value of that attribute. If one or more
// option-value pairs are specified, then the command modifies the given named
// font to have the given values; in this case, all widgets using that font
// will redisplay themselves using the new attributes for the font. See "Font
// Options" below for a list of the possible attributes.
//
// Note that on Aqua/Mac OS X, the system fonts (see "Platform Specific Fonts"
// below) may not be actually altered because they are implemented by the
// system theme. To achieve the effect of modification, use font actual to get
// their configuration and font create to synthesize a copy of the font which
// can be modified.
//
// 	create ?fontname? ?option value ...?
//
// Creates a new named font and returns its name. fontname specifies the name
// for the font; if it is omitted, then Tk generates a new name of the form
// fontx, where x is an integer. There may be any number of option-value pairs,
// which provide the desired attributes for the new named font. See "Font
// Options" below for a list of the possible attributes.
//
// 	delete fontname ?fontname ...?
//
// Delete the specified named fonts. If there are widgets using the named font,
// the named font will not actually be deleted until all the instances are
// released. Those widgets will continue to display using the last known values
// for the named font. If a deleted named font is subsequently recreated with
// another call to font create, the widgets will use the new named font and
// redisplay themselves using the new attributes of that font.
//
// 	families ?-displayof window?
//
// The return value is a list of the case-insensitive names of all font
// families that exist on window's display. If the window argument is omitted,
// it defaults to the main window.
//
// 	measure font ?-displayof window? text
//
// Measures the amount of space the string text would use in the given font
// when displayed in window. font is a font description; see "Font Descriptions"
// below. If the window argument is omitted, it defaults to the main window.
// The return value is the total width in pixels of text, not including the
// extra pixels used by highly exaggerated characters such as cursive “f”. If
// the string contains newlines or tabs, those characters are not expanded or
// treated specially when measuring the string.
//
// 	metrics font ?-displayof window? ?option?
//
// Returns information about the metrics (the font-specific data), for font
// when it is used on window's display. font is a font description; see "Font
// Descriptions" below. If the window argument is omitted, it defaults to the
// main window. If option is specified, returns the value of that metric; if it
// is omitted, the return value is a list of all the metrics and their values.
// See FONT METRICS below for a list of the possible metrics.
//
// 	names
//
// The return value is a list of all the named fonts that are currently defined.
//
// Font Descriptions
//
// The following formats are accepted as a font description anywhere font is
// specified as an argument above; these same forms are also permitted when
// specifying the -font option for widgets.
//
// 	[1] fontname
//
// The name of a named font, created using the font create command. When a
// widget uses a named font, it is guaranteed that this will never cause an
// error, as long as the named font exists, no matter what potentially invalid
// or meaningless set of attributes the named font has. If the named font
// cannot be displayed with exactly the specified attributes, some other close
// font will be substituted automatically.
//
// 	[2] systemfont
//
// The platform-specific name of a font, interpreted by the graphics server.
// This also includes, under X, an XLFD (see [4]) for which a single “*”
// character was used to elide more than one field in the middle of the name.
// See "Platform Specific Fonts" for a list of the system fonts.
//
// 	[3] family ?size? ?style? ?style ...?
//
// A properly formed list whose first element is the desired font family and
// whose optional second element is the desired size. The interpretation of the
// size attribute follows the same rules described for -size in FONT OPTIONS
// below. Any additional optional arguments following the size are font styles.
// Possible values for the style arguments are as follows:
//
// 	normal
// 	bold
// 	roman
// 	italic
// 	underline
// 	overstrike
// .
//
// 	[4] X-font names (XLFD)
//
// A Unix-centric font name of the form
// -foundry-family-weight-slant-setwidth-addstyle-pixel-point-resx-resy-spacing-width-charset-encoding.
// The “*” character may be used to skip individual fields that the user does
// not care about. There must be exactly one “*” for each field skipped, except
// that a “*” at the end of the XLFD skips any remaining fields; the shortest
// valid XLFD is simply “*”, signifying all fields as defaults. Any fields that
// were skipped are given default values. For compatibility, an XLFD always
// chooses a font of the specified pixel size (not point size); although this
// interpretation is not strictly correct, all existing applications using
// XLFDs assumed that one “point” was in fact one pixel and would display
// incorrectly (generally larger) if the correct size font were actually used.
//
// 	[5] option value ?option value ...?
//
// A properly formed list of option-value pairs that specify the desired
// attributes of the font, in the same format used when defining a named font;
// see "Font Options" below.
//
// When font description font is used, the system attempts to parse the
// description according to each of the above five rules, in the order
// specified. Cases [1] and [2] must match the name of an existing named font
// or of a system font. Cases [3], [4], and [5] are accepted on all platforms
// and the closest available font will be used. In some situations it may not
// be possible to find any close font (e.g., the font family was a garbage
// value); in that case, some system-dependent default font is chosen. If the
// font description does not match any of the above patterns, an error is
// generated.
//
// Font Metrics
//
// The following options are used by the font metrics command to query
// font-specific data determined when the font was created. These properties
// are for the whole font itself and not for individual characters drawn in
// that font. In the following definitions, the “baseline” of a font is the
// horizontal line where the bottom of most letters line up; certain letters,
// such as lower-case “g” stick below the baseline.
//
// 	-ascent
//
// The amount in pixels that the tallest letter sticks up above the baseline of
// the font, plus any extra blank space added by the designer of the font.
//
// 	-descent
//
// The largest amount in pixels that any letter sticks down below the baseline
// of the font, plus any extra blank space added by the designer of the font.
//
// 	-linespace
//
// Returns how far apart vertically in pixels two lines of text using the same
// font should be placed so that none of the characters in one line overlap any
// of the characters in the other line. This is generally the sum of the ascent
// above the baseline line plus the descent below the baseline.
//
// 	-fixed
//
// Returns a boolean flag that is “1” if this is a fixed-width font, where each
// normal character is the same width as all the other characters, or is “0” if
// this is a proportionally-spaced font, where individual characters have
// different widths. The widths of control characters, tab characters, and
// other non-printing characters are not included when calculating this value.;
//
// Font Options
//
// The following options are supported on all platforms, and are used when
// constructing a named font or when specifying a font using style [5] as
// above:
//
// 	-family name
//
// The case-insensitive font family name. Tk guarantees to support the font
// families named Courier (a monospaced “typewriter” font), Times (a serifed
// “newspaper” font), and Helvetica (a sans-serif “European” font). The most
// closely matching native font family will automatically be substituted when
// one of the above font families is used. The name may also be the name of a
// native, platform-specific font family; in that case it will work as desired
// on one platform but may not display correctly on other platforms. If the
// family is unspecified or unrecognized, a platform-specific default font will
// be chosen.
//
// 	-size size
//
// The desired size of the font. If the size argument is a positive number, it
// is interpreted as a size in points. If size is a negative number, its
// absolute value is interpreted as a size in pixels. If a font cannot be
// displayed at the specified size, a nearby size will be chosen. If size is
// unspecified or zero, a platform-dependent default size will be chosen.
//
// Sizes should normally be specified in points so the application will remain
// the same ruler size on the screen, even when changing screen resolutions or
// moving scripts across platforms. However, specifying pixels is useful in
// certain circumstances such as when a piece of text must line up with respect
// to a fixed-size bitmap. The mapping between points and pixels is set when
// the application starts, based on properties of the installed monitor, but it
// can be overridden by calling the tk scaling command.
//
// 	-weight weight
//
// The nominal thickness of the characters in the font. The value normal
// specifies a normal weight font, while bold specifies a bold font. The
// closest available weight to the one specified will be chosen. The default
// weight is normal.
//
// 	-slant slant
//
// The amount the characters in the font are slanted away from the vertical.
// Valid values for slant are roman and italic. A roman font is the normal,
// upright appearance of a font, while an italic font is one that is tilted
// some number of degrees from upright. The closest available slant to the one
// specified will be chosen. The default slant is roman.
//
// 	-underline boolean
//
// The value is a boolean flag that specifies whether characters in this font
// should be underlined. The default value for underline is false.
//
// 	-overstrike boolean
//
// The value is a boolean flag that specifies whether a horizontal line should
// be drawn through the middle of characters in this font. The default value
// for overstrike is false.
//
// Standard Fonts
//
// The following named fonts are supported on all systems, and default to
// values that match appropriate system defaults.
//
// 	TkDefaultFont
//
// This font is the default for all GUI items not otherwise specified.
//
// 	TkTextFont
//
// This font should be used for user text in entry widgets, listboxes etc.
//
// 	TkFixedFont
//
// This font is the standard fixed-width font.
//
// 	TkMenuFont
//
// This font is used for menu items.
//
// 	TkHeadingFont
//
// This font should be used for column headings in lists and tables.
//
// 	TkCaptionFont
//
// This font should be used for window and dialog caption bars.
//
// 	TkSmallCaptionFont
//
// This font should be used for captions on contained windows or tool dialogs.
//
// 	TkIconFont
//
// This font should be used for icon captions.
//
// 	TkTooltipFont
//
// This font should be used for tooltip windows (transient information windows).
//
// It is not advised to change these fonts, as they may be modified by Tk
// itself in response to system changes. Instead, make a copy of the font and
// modify that.
//
// Platform Specific Fonts
//
// The following system fonts are supported:
//
// X Windows: All valid X font names, including those listed by xlsfonts(1),
// are available.
//
// MS Windows: The following fonts are supported, and are mapped to the user's
// style defaults.
//
// 	system
// 	ansi
// 	device
// 	systemfixed
// 	ansifixed
// 	oemfixed
//
// Mac OS X: The following fonts are supported, and are mapped to the user's
// style defaults.
//
// 	system
// 	application
// 	menu
//
// Additionally, the following named fonts provide access to the Aqua theme
// fonts:
//
// 	systemSystemFont
// 	systemEmphasizedSystemFont
// 	systemSmallSystemFont
// 	systemSmallEmphasizedSystemFont
// 	systemApplicationFont
// 	systemLabelFont
// 	systemViewsFont
// 	systemMenuTitleFont
// 	systemMenuItemFont
// 	systemMenuItemMarkFont
// 	systemMenuItemCmdKeyFont
// 	systemWindowTitleFont
// 	systemPushButtonFont
// 	systemUtilityWindowTitleFont
// 	systemAlertHeaderFont
// 	systemToolbarFont
// 	systemMiniSystemFont
// 	systemDetailSystemFont
// 	systemDetailEmphasizedSystemFont
func (a *Application) Font(s string) (string, error) { return a.eval(s) }

// FontM is like Font but panics on errors.
func (a *Application) FontM(s string) string {
	s, err := a.Font(s)
	if err != nil {
		panic(err)
	}

	return s
}

// Bitmap wraps the Tk image create bitmap command.
//
// A bitmap is an image whose pixels can display either of two colors or be
// transparent. A bitmap image is defined by four things: a background color, a
// foreground color, and two bitmaps, called the source and the mask. Each of
// the bitmaps specifies 0/1 values for a rectangular array of pixels, and the
// two bitmaps must have the same dimensions. For pixels where the mask is
// zero, the image displays nothing, producing a transparent effect. For other
// pixels, the image displays the foreground color if the source data is one
// and the background color if the source data is zero.
//
// Bitmaps support the following options:
//
// 	-background color
//
// Specifies a background color for the image in any of the standard ways
// accepted by Tk. If this option is set to an empty string then the background
// pixels will be transparent. This effect is achieved by using the source
// bitmap as the mask bitmap, ignoring any -maskdata or -maskfile options.
//
// 	-data string
//
// Specifies the contents of the source bitmap as a string. The string must
// adhere to X11 bitmap format (e.g., as generated by the bitmap program). If
// both the -data and -file options are specified, the -data option takes
// precedence.
//
// 	-file name
//
// name gives the name of a file whose contents define the source bitmap. The
// file must adhere to X11 bitmap format (e.g., as generated by the bitmap
// program).
//
// 	-foreground color
//
// Specifies a foreground color for the image in any of the standard ways
// accepted by Tk.
//
// 	-maskdata string
//
// Specifies the contents of the mask as a string. The string must adhere to
// X11 bitmap format (e.g., as generated by the bitmap program). If both the
// -maskdata and -maskfile options are specified, the -maskdata option takes
// precedence.
//
// 	-maskfile name
//
// name gives the name of a file whose contents define the mask. The file must
// adhere to X11 bitmap format (e.g., as generated by the bitmap program).
func (a *Application) Bitmap(s string) (string, error) { return a.eval("image create bitmap " + s) }

// BitmapM is like Bitmap but panics on errors.
func (a *Application) BitmapM(s string) string {
	s, err := a.Bitmap(s)
	if err != nil {
		panic(err)
	}

	return s
}

// Destroy deletes the widgets given by the w arguments, plus all of their
// descendants. If the root window from (*Application).Root is deleted then all
// widgets will be destroyed and the application will (normally) exit. The
// widgets are destroyed in order, and if an error occurs in destroying a
// widget the command aborts without destroying the remaining widgets. No error
// is returned if widget does not exist.
func (a *Application) Destroy(w ...Widget) error {
	_, err := a.eval("destroy " + winList(w))
	return err
}

// DestroyM is like Destroy but panics on errors.
func (a *Application) DestroyM(w ...Widget) *Application {
	if err := a.Destroy(w...); err != nil {
		panic(err)
	}

	return a
}

func winList(w []Widget) string {
	var a []string
	for _, v := range w {
		a = append(a, v.Win().path)
	}

	return strings.Join(a, " ")
}

func (a *Application) newCommand(w Widget, cmd Command) (string, error) {
	sid := "::go::command" + a.newID()
	_, err := a.interp.NewCommand(
		sid,
		func(clientData interface{}, in *tcl.Interp, args []string) int {
			if err := cmd(w, args); err != nil {
				return tcllib.TCL_ERROR
			}

			return tcllib.TCL_OK
		},
		nil,
		nil,
	)
	if err != nil {
		return "", err
	}

	return sid, nil
}

// Pack is equivalent to PackConfigure with no options.
func (a *Application) Pack(w ...Widget) error {
	_, err := a.eval("pack " + winList(w))
	return err
}

// PackM is like pack but panics on error.
func (a *Application) PackM(w ...Widget) *Application {
	if err := a.Pack(w...); err != nil {
		panic(err)
	}

	return a
}

// Command is a Go function used by the Command option of some widgets.
type Command func(w Widget, args []string) error

func tclBool2bool(s string) (bool, error) {
	switch s {
	case "1", "true", "yes":
		return true, nil
	case "0", "false", "no":
		return false, nil
	default:
		return false, fmt.Errorf("unrecognized Tcl boolean: %q", s)
	}
}

// Window is the base of all GUI elements.
type Window struct {
	app  *Application
	path string
}

// Win implements Widget.
func (w *Window) Win() *Window { return w }

func (w *Window) eval(s string) (string, error)       { return w.app.eval(s) }
func (w *Window) evalBool(s string) (bool, error)     { return w.app.evalBool(s) }
func (w *Window) evalFloat(s string) (float64, error) { return w.app.evalFloat(s) }
func (w *Window) evalInt(s string) (int, error)       { return w.app.evalInt(s) }

func (w *Window) initChild(ch *Window) {
	ch.app = w.app
	switch w.path {
	case ".":
		ch.path = w.path + w.app.newID()
	default:
		ch.path = w.path + "." + w.app.newID()
	}
}

// Widget represents any GUI element.
type Widget interface {
	Win() *Window
}

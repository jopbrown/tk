module modernc.org/tk

go 1.17

require (
	github.com/cosmos72/gls v0.0.0-20180519201422-29add83bde4c
	modernc.org/ccgo/v3 v3.12.95
	modernc.org/fontconfig v1.0.7
	modernc.org/libc v1.11.104
	modernc.org/strutil v1.1.1
	modernc.org/tcl v1.9.2
	modernc.org/x11 v1.0.12
	modernc.org/xft v1.0.6
)

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20200410134404-eec4a21b6bb0 // indirect
	golang.org/x/mod v0.3.0 // indirect
	golang.org/x/sys v0.0.0-20211007075335-d3039528d8ac // indirect
	golang.org/x/tools v0.0.0-20201124115921-2c860bdd6e78 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	lukechampine.com/uint128 v1.1.1 // indirect
	modernc.org/cc/v3 v3.35.18 // indirect
	modernc.org/expat v1.0.8 // indirect
	modernc.org/freetype v1.0.6 // indirect
	modernc.org/gettext v0.0.12 // indirect
	modernc.org/httpfs v1.0.6 // indirect
	modernc.org/mathutil v1.4.1 // indirect
	modernc.org/memory v1.0.5 // indirect
	modernc.org/opt v0.1.1 // indirect
	modernc.org/token v1.0.0 // indirect
	modernc.org/xau v1.0.13 // indirect
	modernc.org/xcb v1.0.12 // indirect
	modernc.org/xdmcp v1.0.14 // indirect
	modernc.org/xrender v1.0.6 // indirect
	modernc.org/z v1.2.20 // indirect
)

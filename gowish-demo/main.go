// Copyright 2021 The Tk-Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"

	"modernc.org/libc"
	"modernc.org/tcl"
	"modernc.org/tk/internal/lib"
	"modernc.org/tk/internal/wish"
)

const (
	tclLibrary = "TCL_LIBRARY"
	tkLibrary  = "TK_LIBRARY"
)

func main() {
	runtime.LockOSThread()
	tclDir, err := ioutil.TempDir("", "gowish-demo")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	if err := tcl.Library(tclDir); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	libc.AtExit(func() { os.RemoveAll(tclDir) })
	os.Setenv(tclLibrary, tclDir)

	tkDir, err := ioutil.TempDir("", "gowish-demo")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	if err := lib.Library(tkDir); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	libc.AtExit(func() { os.RemoveAll(tkDir) })
	os.Setenv(tkLibrary, tkDir)
	if len(os.Args) == 1 {
		os.Args = append(os.Args, filepath.Join(tkDir, "demos", "widget"))
	}
	libc.Start(wish.Main)
}

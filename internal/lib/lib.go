// Copyright 2021 The Tk-Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package lib

import (
	"os"
	"path/filepath"
	"sort"
	"strings"
)

// Assets provide the tk library as a mapping of paths to content.
func Assets() map[string]string { return assets }

// Library writes the Tk library to directory.
func Library(directory string) error {
	var a []string
	for k := range assets {
		a = append(a, k)
	}
	sort.Strings(a)
	dirs := map[string]struct{}{}
	for _, nm := range a {
		pth := filepath.Join(directory, filepath.FromSlash(nm))
		dir := pth
		if !strings.HasSuffix(nm, "/") {
			dir = filepath.Dir(pth)
		}
		if _, ok := dirs[dir]; !ok {
			if err := os.MkdirAll(dir, 0755); err != nil {
				return err
			}

			dirs[dir] = struct{}{}
		}
		if strings.HasSuffix(nm, "/") {
			continue
		}

		f, err := os.Create(pth)
		if err != nil {
			return err
		}

		if _, err := f.Write([]byte(assets[nm])); err != nil {
			f.Close()
			return err
		}

		if err = f.Close(); err != nil {
			return err
		}
	}
	return nil
}
